#include <SDL.h>
#include <stdio.h>
#include <fstream>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;

const int MAX_MAP_Y = 15;
const int MAX_MAP_X = 20;

typedef struct Map
{
    int tile[MAX_MAP_Y][MAX_MAP_X];
} Map;

Map loadMap(char *name, Map board)
{
    int x,y;
    FILE *fp;

    fp = fopen(name, "rb");

    for(y = 0; y < MAX_MAP_Y; y++)
    {
        for(x = 0; x < MAX_MAP_X; x++)
            board.tile[y][x] = 0;
    }

    if(fp == NULL)
    {
        printf("Failed to open file %s \n", name);
    }

    for(y = 0; y < MAX_MAP_Y; y++)
    {
        for(x = 0; x < MAX_MAP_X; x++)
        {
            fscanf(fp, "%d", &board.tile[y][x]);
        }
    }
    fclose(fp);
    return board;
}

void drawMap(SDL_Renderer *renderer, Map board  )
{
    int x, y;


    for(y = 0; y < MAX_MAP_Y; y++)
    {
        for(x = 0; x < MAX_MAP_X; x++)
        {
            if (board.tile[y][x] == 1)
            {
                SDL_Rect wall = {(x*TILE_WIDTH), (y*TILE_HEIGHT), TILE_HEIGHT, TILE_HEIGHT};
                SDL_SetRenderDrawColor(renderer, 0 ,0 ,0 , 255);
                SDL_RenderFillRect(renderer, &wall);

            }
            else
            {
                SDL_Rect boundary = {(x*TILE_WIDTH) , (y*TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT};
                SDL_SetRenderDrawColor(renderer, 154, 154, 154, 255);
                SDL_RenderFillRect(renderer, &boundary);
            }

        }
        SDL_RenderPresent(renderer);
    }
}


int main(int argc, char* argv[])
{
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
    Map board;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Not initialize");
    }
    else
    {
        window = SDL_CreateWindow("Tile Board", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if(window == NULL)
        {
            printf("window could not be created");
        }
        else
        {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
            board = loadMap("Map.txt", board);

            drawMap(renderer, board);
        }
    }

    SDL_Event e;
    int quit = 0;
    while (!quit)
    {
        if (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
                quit = 1;
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
